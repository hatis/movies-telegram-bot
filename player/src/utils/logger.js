function log(level, message, data) {
    console.error(level, message, data)

    const browser = `${navigator.userAgent}`

    if (!location.host.startsWith('localhost') && !window.mobileApp) {
        fetch(`${window.API_BASE_URL}/log`, {
            method: 'POST',
            credentials: 'same-origin',
            cache: 'no-cache',
            redirect: 'follow',
            referrer: 'no-referrer',
            body: JSON.stringify({
                level,
                message,
                data: { ...data, browser }
            })
        })
    }
}

export default {
    error: (message, data) => {
        log('ERROR', message, data)
    },
    info: (message, data) => {
        log('INFO', message, data)
    }
}