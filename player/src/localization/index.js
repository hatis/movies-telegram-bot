import LocalizedStrings from 'react-localization';

export default new LocalizedStrings({
    en: {
        cantLoadPLaylist: 'Can`t load playlist',
        videoNotFound: 'Video not found',
        cantPlayMedia: 'Could not play media',
        urlCopied: 'URL copied!',
        shareWith: 'Share with...',
        curPlaylistPos: 'Current playlist position',
        curTimePos: 'Current time',
        shuffleOn: 'Shuffle playlist mode ON',
        shuffleOff: 'Shuffle playlist mode OFF',
        searchAlternatives: '{0}<br/><a id="altenativeLink" href="{1}">Search alternative links</a>',
        moviesBotTip: 'movies and tv shows',
        animeBotTip: 'search anime'
    },
    ru: {
        cantLoadPLaylist: 'Ошибка загрузки',
        cantPlayMedia: 'Невозможно возпроизвести видео',
        videoNotFound: 'Видео недоступно',
        urlCopied: 'URL скопирован',
        shareWith: 'Поделится в...',
        curPlaylistPos: 'Серия',
        curTimePos: 'Время',
        shuffleOn: 'Случаный режим включен',
        shuffleOff: 'Случаный режим выключен',
        searchAlternatives: '{0}<br/><a id="altenativeLink" href="{1}">Поискать другие ссылки</a>',
        moviesBotTip: 'фильмы и сериалы',
        animeBotTip: 'поиск аниме'
    }
})