import playerStore from './player-store'
import notificationStore from './notifications-store'
import castStore from './cast-store'

export default {
    notificationStore,
    playerStore,
    castStore
}